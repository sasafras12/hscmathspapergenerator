#ifndef _FILEIO_H
#define _FILEIO_H

#include <fstream>

void write_to_file(std::string filename, std::string latex)
{
	std::ofstream file;
	file.open(filename);
	file << latex;
	file.close();
}

void latex_to_pdf(std::string filename) {
	std::string pdflatex_cmd = "/usr/bin/pdflatex";
	pdflatex_cmd.append(" ");
	pdflatex_cmd.append(filename);
	pdflatex_cmd.append(" -output-directory temp");
	system(pdflatex_cmd.c_str());
}
#endif