#ifndef _DOCUMENT_H
#define _DOCUMENT_H

#include <vector>
#include "fileio.h"
#include "question.h"


class Document {

std::string name;
std::vector<std::string> questions;
std::vector<std::string> answers;
std::vector<std::string> preamble;
std::vector<std::string> document_boilerplate;
std::string tex;

public:
	Document(std::string name) : name(name)
	{
		document_boilerplate.push_back("\\documentclass[12pt]{article}\n");
		preamble.push_back("\\usepackage{amsmath}\n");
		document_boilerplate.push_back("\\begin{document}\n");
		document_boilerplate.push_back("\\end{document}\n");
	}
	std::string getDocument();
	void addQuestion(Question q);
};

void Document::addQuestion(Question q) {
	questions.push_back(q.getQuestion());
	answers.push_back(q.getAnswer());
}

std::string Document::getDocument() {

	tex.append(document_boilerplate.at(0));
	for (auto s : preamble)
		tex.append(s);
	tex.append(document_boilerplate.at(1));
	for (auto s : questions)
		tex.append(s);
	for (auto s : answers)
		tex.append(s);
	tex.append(document_boilerplate.at(2));

	return tex;
}

#endif