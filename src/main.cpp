#include <iostream>
#include "document.h"

int main(int argc, char *argv[])
{
	std::string path = argv[1];
	std::string filename = "main.tex";
	std::string dir = path + filename;
	Document hello("helloWorld");
	Question hello_world("hello world!");
	hello.addQuestion(hello_world);
	write_to_file(dir, hello.getDocument());
	latex_to_pdf(dir);
	return 0;
}