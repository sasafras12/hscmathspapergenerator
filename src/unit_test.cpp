#include <iostream>
#include "document.h"
int main(int argc, char *argv[])
{
	std::string filename = "helloworld.tex";
	Document hello("helloWorld");
	Question hello_world("hello world!");
	hello.addQuestion(hello_world);
	write_to_file(filename, hello.getDocument());
	latex_to_pdf(filename);
	return 0;
}
