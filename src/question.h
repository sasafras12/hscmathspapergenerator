#ifndef _Question_H
#define _Question_H

class Question {

std::string query;
std::string answer;
std::pair<std::string, std::string> question_boilerplate;
std::pair<std::string, std::string> answer_boilerplate;

public:
	Question(std::string query) : query(query) {}
	std::string getQuestion() { return question_boilerplate.first + query + question_boilerplate.second; }
	std::string getAnswer() { return answer_boilerplate.first + query + answer_boilerplate.second; }
};

#endif